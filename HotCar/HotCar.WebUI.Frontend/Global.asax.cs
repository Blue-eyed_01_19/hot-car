﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using HotCar.BLL;
using HotCar.Entities;
using HotCar.Repositories.Sql;
using HotCar.WebUI.Frontend.App_Start;
using HotCar.WebUI.Frontend.Code;
using HotCar.WebUI.Frontend.Controllers;

namespace HotCar.WebUI.Frontend
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);          
        }
       
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {                                      
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;
                        bool status;

                        var securityManager =
                            new SecurityManager(
                                new UserRepository(
                                    ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString));

                        roles = securityManager.ReadUserRole(username, out status);

                        if (status)
                        {
                            securityManager.SignOut();
                        }
                        else
                        {
                            //Let us set the Pricipal with our user specific details
                            HttpContext.Current.User = new GenericPrincipal(
                              new GenericIdentity(username, "Forms"), roles.Split(';'));
                        }
                        
                    }
                    catch (Exception)
                    {
                        //somehting went wrong
                    }
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var httpContext = ((MvcApplication)sender).Context;          
            var ex = Server.GetLastError();
            var controller = new ErrorController();
            var routeData = new RouteData();
            var action = "ServerError";

            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Fatal(ex);

            if (ex is HttpException)
            {
                return;
            }
            
            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = 500;
            httpContext.Response.TrySkipIisCustomErrors = true;

            routeData.Values["controller"] = "Error";
            routeData.Values["exception"] = ex;
            routeData.Values["action"] = action;

            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
        }

        protected void Application_EndRequest()
        {
            if (Server.GetLastError() != null)
            {
                var ex = Server.GetLastError();
                if (ex is HttpException)
                {
                    var controller = new ErrorController();
                    var httpException = ex as HttpException;
                    Response.Clear();
                    Server.ClearError();
                    var routeData = new RouteData();
                    routeData.Values["controller"] = "Error";
                    routeData.Values["exception"] = ex;

                    Response.StatusCode = httpException.GetHttpCode();
                    Response.TrySkipIisCustomErrors = true;

                    switch (Response.StatusCode)
                    {
                        case 401:
                            routeData.Values["action"] = "AccessDenied";
                            break;
                        case 404:
                            routeData.Values["action"] = "NotFound";
                            break;
                        case 500:
                            routeData.Values["action"] = "ServerError";
                            break;
                    }

                    ((IController) controller).Execute(new RequestContext(new HttpContextWrapper(Context), routeData));

                }
            }
        }
    }
}

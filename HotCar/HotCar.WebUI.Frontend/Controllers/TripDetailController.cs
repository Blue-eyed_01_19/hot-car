﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HotCar.BLL.Abstract;
using HotCar.Entities;
using HotCar.WebUI.Frontend.Code.ControllerExtensions;
using HotCar.WebUI.Frontend.Models;
using Newtonsoft.Json;

namespace HotCar.WebUI.Frontend.Controllers
{
    public class TripDetailController : Controller
    {
        #region ApplyToTripStatusString

        private const string APPLY_ERROR_PLACES = "недоступна кількість місць. Оновіть сторінку та повторіть спробу ще раз";
        
        private const string APPLY_OWNER_ERROR = "Ви не можете бути пасажиром власної поїздки";

        private const string APPLY_AGE_ERROR =
            "особи, які не досягли 18-и річного віку, не можуть реєструватись на поїздку";

        private const string APPLY_SUCCESS = "ваша заявка прийнята; очікуйте підтвердження від водія";

        #endregion

        #region Fields

        private readonly ITripManager _tripManager;
        private readonly ISecurityManager _securityManager;
        private readonly IUsersManager _userManager;
        private readonly ICommentManager _commentManager;
        private readonly IApplManager _applManager;

        #endregion

        #region Constructor

        public TripDetailController(IUsersManager userManager, ITripManager tripManager, 
            ICommentManager commentManager, ISecurityManager securityManager, IApplManager applManager)
        {
            this._userManager = userManager;
            this._tripManager = tripManager;
            this._commentManager = commentManager;
            this._securityManager = securityManager;
            this._applManager = applManager;
        }

        #endregion

        #region Actions

        [Authorize]
        [HttpGet]
        public ActionResult GetTripInfo(int tripId)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this._securityManager.GetName());        
            userModel.Trips.Add(this._tripManager.GetTripById(tripId));

            var driver = this._userManager.GetUser(userModel.Trips[0].Driver.Login); 
            userModel.Trips[0].Driver.Rating = this._commentManager.GetDriverRating(driver.Id);
            foreach (var x in userModel.Trips)
            {
                _tripManager.ClearPassengersFromTrip(x, false);
            }

            return View("GetTripInfo", userModel);
        }

        public ActionResult DriverComments(string login)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(login);
            userModel.UserInfo.Rating = this._commentManager.GetDriverRating(userModel.UserInfo.Id);
            userModel.Comments = this._commentManager.GetDriverComments(userModel.UserInfo.Id);
                   
            return Json(new
            {                
                htmlContent = this.RenderPartialViewToString("Modal/CommentPartial", userModel)
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult PassengerComments(string login)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(login);
            userModel.UserInfo.Rating = this._commentManager.GetPassengerRating(userModel.UserInfo.Id);
            userModel.Comments = this._commentManager.GetPassengerComments(userModel.UserInfo.Id);
            
            return Json(new
            {
                htmlContent = this.RenderPartialViewToString("Modal/CommentPartial", userModel)
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult AddNewComment(int tripId, string commentText, string about, double rating)
        {
            var userName = this._securityManager.GetName();           
            if (!this._commentManager.CreateNewComment(tripId, commentText, about, userName, rating))
            {
                return this.PassengerComments(about);
            }
            else
            {
                return this.DriverComments(about);
            }
                        
        }

        [Authorize]
        [HttpPost]
        [Authorize]
        public ActionResult SignToTrip(int tripId, byte countReservedSeats, int cost)
        {
            Passenger passenger = new Passenger()
            {
                UserInfo = _userManager.GetUser(User.Identity.Name),
                TripId = tripId,
                CountReservedSeats = countReservedSeats,
                Cost = cost
            };

            Trip trip = _tripManager.GetTripById(tripId);

            string applMessage;
            bool success = ChackPassengerAppl(passenger, trip, out applMessage);
          
            if (success)
            {
                _applManager.SignToTrip(passenger);
            }

            return Json(new
            {
                success = success,
                applMessage = applMessage
            });
        }

        #endregion

        #region Helpers

        bool ChackPassengerAppl(Passenger p, Trip t, out string applMessage)
        {
            if (p.UserInfo.Age < 18)
            {
                applMessage = APPLY_AGE_ERROR;
                return false;
            }
            else if (User.Identity.Name == t.Driver.Login)
            {
                applMessage = APPLY_OWNER_ERROR;
                return false;
            }
            else if (p.CountReservedSeats > t.AvailablePlacesCount)
            {
                applMessage = APPLY_ERROR_PLACES;
                return false;
            }

            applMessage = APPLY_SUCCESS;
            return true;
        }

        #endregion
    }
}
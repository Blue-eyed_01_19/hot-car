﻿$(document).ready(function () {

	var MenuClickActions = function () {
		var isOnSettingsMenu = false;
		var isOnTripsMenu = false;
		var isOnApplMenu = false;
		var isOnCommnetsMenu = false;

		var onMenuClick = function (status, $menuElement, $contentElement) {
			var scrollTimeMs = 1000;
			$menuElement.click(function () {

				$contentElement.slideToggle('slow');
				$('html, body').animate({
					scrollTop: $contentElement.offset().top - 100
				}, scrollTimeMs);

				if (status === false) {
					$menuElement.addClass('selected-menu');
					$menuElement.css('color', 'lightgrey');
					status = true;
				} else {
					$menuElement.removeClass('selected-menu');
					$menuElement.css('color', 'white');
					status = false;
				}
			});
		}
		this.init = function () {
			onMenuClick(isOnSettingsMenu, $('#settings-menu'), $('#settings-content'));
			onMenuClick(isOnTripsMenu, $('#trips-menu'), $('#trips-content'));
			onMenuClick(isOnApplMenu, $('#appl-menu'), $('#appl-content'));
			onMenuClick(isOnCommnetsMenu, $('#comments-menu'), $('#comments-content'));

			$('#comments-menu').on("click", function () { $("#tab-count-comments-about-me").click(); });
		}
	}

	var TripsActions = function () {
		var tr;
		var tripsContent;

		var isTabActualTripsOn = false;
		var isTabOutdatedTripsOn = false;

		function refreshActualTripsInfo(countActualTrips) {
			$('#actual-trips-loading').css('display', 'none');
			$('#count-actual-trips').text(countActualTrips);
			$('#tab-count-actual-trips').text(countActualTrips);
		}

		function refreshOutDatedTrips(countOutDatedTrips) {
			$('#outdated-trips-loading').css('display', 'none');
			$('#count-outdated-trips').text(countOutDatedTrips);
			$('#tab-count-oudated-trips').text(countOutDatedTrips);
		}

		this.init = function (trips) {
			tr = trips;
			tripsContent = new Content($('#trips-region'));
			$("#tab-actual-trips").click(function () {
				if (isTabActualTripsOn === false) {
					tripsContent.refillContent(tr.getActualTripHtml(), false);
					isTabActualTripsOn = true;
					isTabOutdatedTripsOn = false;
				}
			});

			$("#tab-outdated-trips").click(function () {
				if (isTabOutdatedTripsOn === false) {
					tripsContent.refillContent(tr.getOutDatedTripHtml(), false);
					isTabOutdatedTripsOn = true;
					isTabActualTripsOn = false;
				}
			});

			$('#trips-menu').on("click", function () { $("#tab-actual-trips").click(); });

			tr.getActualTrips(function () {
				refreshActualTripsInfo(tr.getCountActualTrips());
			});

			tr.getOutDatedTrips(function () {
				refreshOutDatedTrips(tr.getCountOutDatedTrips());
			});
		}

		this.refreshActualTrips = function() {
			tr.getActualTrips(function() {
				if (isTabActualTripsOn) {
					tripsContent.refillContent(tr.getActualTripHtml(), false);
				}
			});
		}
	}

	var CommentsActions = function () {
		var cm;
		var commentsContent;

		var isTabMyCommentsOn = false;
		var isTabCommentsAboutMeOn = false;

		function refreshMyCommentsInfo(count) {
			$('#tab-count-my-comments').text(count);
		}

		function refreshCommentsAboutMeInfo(count) {
			$('#users-comments-loading').css('display', 'none');
			$('#count-users-comments').text(count);
			$('#tab-count-comments-about-me').text(count);
		}

		this.init = function (comments) {
			cm = comments;
			commentsContent = new Content($('#comments-region'));

			cm.getMyComments(function () {
				refreshMyCommentsInfo(cm.getCountMyComments);
			});

			cm.getCommentsAboutMe(function () {
				refreshCommentsAboutMeInfo(cm.getCountCommentsAboutMe);
			});

			$("#tab-my-comments").click(function () {
				if (isTabMyCommentsOn === false) {
					commentsContent.refillContent(cm.getMyCommentsHtml(), false);
					isTabMyCommentsOn = true;
					isTabCommentsAboutMeOn = false;
				}
			});

			$("#tab-comments-about-me").click(function () {
				if (isTabCommentsAboutMeOn === false) {
					commentsContent.refillContent(cm.getCommentsAboutMeHtml(), false);
					isTabCommentsAboutMeOn = true;
					isTabMyCommentsOn = false;
				}
			});
		}
	}

	var ApplsActions = function () {
		var ap;
		var applContent;

		function refreshNewAppl(countNewAppls, slider) {
			$('#new-appls-loading').css('display', 'none');
			$('#count-new-appls').text(countNewAppls);
			applContent.refillContent(ap.getApplsHtml(), slider);
		}

		function refillAppls() {
			ap.getNewAppl(function () {
				refreshNewAppl(ap.getCountNewAppl(), false);
			});
		}

		this.init = function (appls) {
			ap = appls;
			applContent = new Content($('#appl-content'));
			refillAppls();
		}

		this.refreshAppls = function () {
			refillAppls();
		}
	}

	var SettingsActions = function () {
		this.init = function () {
			$("#password-settings").attr({ "href": "#", "data-toggle": "modal", "data-target": "#PasswordSettings" });
			$("#name-settings").attr({ "href": "#", "data-toggle": "modal", "data-target": "#FirstNameSettings" });
			$("#about-me-settings").attr({ "href": "#", "data-toggle": "modal", "data-target": "#AboutMeSettings" });
			$("#sur-name-settings").attr({ "href": "#", "data-toggle": "modal", "data-target": "#SurNameSettings" });
			$("#mail-settings").attr({ "href": "#", "data-toggle": "modal", "data-target": "#MailSettings" });
			$("#phone-settings").attr({ "href": "#", "data-toggle": "modal", "data-target": "#PhoneSettings" });
			$("#birthday-settings").attr({ "href": "#", "data-toggle": "modal", "data-target": "#BirthdaySettings" });
			$("#avatar-upload").attr({ "href": "#", "data-toggle": "modal", "data-target": "#AvatarUpload" });
		}
	}


	var menuClickActions = new MenuClickActions();
	menuClickActions.init();

	var trips = new Trips();
	var tripsActions = new TripsActions();
	tripsActions.init(trips);

	var comments = new Comments();
	var commentsActions = new CommentsActions();
	commentsActions.init(comments);

	var appls = new Appl();
	var applsActions = new ApplsActions();
	applsActions.init(appls);

	var settingsActions = new SettingsActions();
	settingsActions.init();


	/************* show modal window about status of apply new appl. *******************/ 
	var applyApplResult = function(success, text) {
		if (success) {
			$('#status-image').attr("src", "../Content/images/FindTrip/ok.png");
			applsActions.refreshAppls();
			tripsActions.refreshActualTrips();
		} else {
			$("#status-image").attr("src", "../Content/images/FindTrip/warning.png");
		}
		$('#working-text').text(text);
	}

	var resetModalContent = function() {
		$('#status-image').attr("src", "../Content/images/UserAccount/loading.gif");
		$('#working-text').text('йде обробка запиту...');
	}

	applyApplToTrip = function (tripId, passengerLogin) {
		resetModalContent();
		$("#ApplyApplModal").modal();
		appls.applyApplToTrip(tripId, passengerLogin, applyApplResult);
	}
	/**********************************************************************************/
});
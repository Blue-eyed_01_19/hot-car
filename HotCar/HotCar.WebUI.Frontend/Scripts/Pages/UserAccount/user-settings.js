﻿$(document).ready(function () {
    changeFirstName = function () {
        var txtFirstName = document.getElementById("contact_name");
        var lblError = document.getElementById("contact_name_error");
        var regEx = new RegExp("^([АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюяA-Za-z]{1,10})$");
        if (!txtFirstName.value.match(regEx)) {
            lblError.className = "";
            lblError.className = lblError.className + " help-block text-danger";
            hideError(lblError);
            return;
        }

        lblError.className = "hidden";

        $.ajax({
            url: "/UserAccount/ChangeFirstName",
            type: "POST",
            dataType: "json",
            data: { firstName: txtFirstName.value }
        }).done(function (data) {
            document.getElementById("firstNameSpan").innerHTML = txtFirstName.value;
            document.getElementById("indexFirstNameSpan").innerHTML = txtFirstName.value;
            var text = document.getElementById("indexTopFirstSurNameSpan").innerHTML;
            var splitted = text.split(' ');
            document.getElementById("indexTopFirstSurNameSpan").innerHTML = txtFirstName.value + " " + splitted[1];
            txtFirstName.value = "";
        });
    }

    changeSurName = function () {
        var txtSurName = document.getElementById("contact_surname");
        var lblError = document.getElementById("contact_surname_error");
        var regEx = new RegExp("^([АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюяA-Za-z]{1,20})$");
        if (!txtSurName.value.match(regEx)) {
            lblError.className = "";
            lblError.className = lblError.className + " help-block text-danger";
            hideError(lblError);
            return;
        }

        lblError.className = "hidden";

        $.ajax({
            url: "/UserAccount/ChangeSurName",
            type: "POST",
            dataType: "json",
            data: { surName: txtSurName.value }
        }).done(function (data) {
            document.getElementById("surNameSpan").innerHTML = txtSurName.value;
            document.getElementById("indexSurNameSpan").innerHTML = txtSurName.value;
            var text = document.getElementById("indexTopFirstSurNameSpan").innerHTML;
            var splitted = text.split(' ');
            document.getElementById("indexTopFirstSurNameSpan").innerHTML = splitted[0] +" " + txtSurName.value;
            txtSurName.value = "";
        });
    }

    changePhone = function () {
        var txtPhone = document.getElementById("contact_phone");
        var lblError = document.getElementById("contact_phone_error");
        var regEx = new RegExp("^\\+([0-9]){12}$");
        if (!txtPhone.value.match(regEx)) {
            lblError.className = "";
            lblError.className = lblError.className + " help-block text-danger";
            hideError(lblError);
            return;
        }

        lblError.className = "hidden";

        $.ajax({
            url: "/UserAccount/ChangePhone",
            type: "POST",
            dataType: "json",
            data: { phone: txtPhone.value }
        }).done(function (data) {
            document.getElementById("phoneSpan").innerHTML = txtPhone.value;
            document.getElementById("indexPhoneSpan").innerHTML = txtPhone.value;
            document.getElementById("indexTopPhoneSpan").innerHTML = txtPhone.value;
            txtPhone.value = "";
        });
    }

    changeMail = function () {
        var txtMail = document.getElementById("contact_email");
        var lblError = document.getElementById("contact_email_error");
        var regEx = new RegExp("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
        if (!txtMail.value.match(regEx)) {
            lblError.className = "";
            lblError.className = lblError.className + " help-block text-danger";
            hideError(lblError);
            return;
        }

        lblError.className = "hidden";

        $.ajax({
            url: "/UserAccount/ChangeMail",
            type: "POST",
            dataType: "json",
            data: { mail: txtMail.value }
        }).done(function (data) {
            if (data.success === false) {
                lblError.className = "";
                lblError.className = lblError.className + " help-block text-danger";
                lblError.innerHTML = "Така електронна пошта вже зареєстрована";
                return;
            }
            else {
                document.getElementById("mailSpan").innerHTML = txtMail.value;
                document.getElementById("indexMailSpan").innerHTML = txtMail.value;
                document.getElementById("indexTopMailSpan").innerHTML = txtMail.value;
                txtMail.value = "";
            }
        });
    }

    changeBirthday = function () {
        var txtBirthday = document.getElementById("contact_birthday");

        $.ajax({
            url: "/UserAccount/ChangeBirthday",
            type: "POST",
            dataType: "json",
            data: { birthday: txtBirthday.value }
        }).done(function (data) {
            document.getElementById("birthdaySpan").innerHTML = txtBirthday.value;
            document.getElementById("indexBirthdaySpan").innerHTML = txtBirthday.value;
            document.getElementById("indexTopBirthdaySpan").innerHTML = txtBirthday.value;
            txtBirthday.value = "";
        });
    }

    changeAbout = function () {
        var txtAbout = document.getElementById("contact_message");
        $.ajax({
            url: "/UserAccount/ChangeAbout",
            type: "POST",
            dataType: "json",
            data: { about: txtAbout.value }
        }).done(function (data) {
            document.getElementById("aboutSpan").innerHTML = txtAbout.value;
            document.getElementById("indexAboutSpan").innerHTML = txtAbout.value;
            txtAbout.value = "";
        });
    }

    changePassword = function () {
        var txtPasswordOld = document.getElementById("input-passwordOld");
        var txtPasswordNew = document.getElementById("input-passwordNew");
        var txtPasswordNewConfirm = document.getElementById("input-passwordNewConfirm");
        var lblError = document.getElementById("contact_password_error");
        var regEx = new RegExp("^.{6,20}$");
        if (!txtPasswordNew.value.match(regEx)) {
            lblError.innerHTML = "Пароль надто короткий або надто довгий (6 - 20 символів)";
            lblError.className = "";
            lblError.className = lblError.className + " help-block text-danger";
            hideError(lblError);
            return;
        }
        if (txtPasswordNew.value !== txtPasswordNewConfirm.value) {
            lblError.innerHTML = "Нові паролі не одинакові";
            lblError.className = "";
            lblError.className = lblError.className + " help-block text-danger";
            hideError(lblError);
            return;
        }
        if (txtPasswordNew.value === txtPasswordOld.value) {
            lblError.innerHTML = "Новий і старий пароль одинакові";
            lblError.className = "";
            lblError.className = lblError.className + " help-block text-danger";
            hideError(lblError);
            return;
        }

        lblError.className = "hidden";

        $.ajax({
            url: "/UserAccount/ChangePassword",
            type: "POST",
            dataType: "json",
            data: { oldPassword: txtPasswordOld.value, newPassword: txtPasswordNew.value, newPasswordConfirm: txtPasswordNewConfirm.value }
        }).done(function (data) {
            if (data.success === true) {
                txtPasswordOld.value = "";
                txtPasswordNew.value = "";
                txtPasswordNewConfirm.value = "";
                $.bootstrapGrowl("Пароль змінено", {
                    type: "success",
                    offset: { from: "top", amount: 200 },
                    align: "center",
                    width: "auto",
                    allow_dismiss: false,
                    delay: 2000
                });
            } else {
                lblError.innerHTML = "Невірний старий пароль";
                lblError.className = "";
                lblError.className = lblError.className + " help-block text-danger";
            }
        });
    }

    function hideError(control) {
        var timer = setInterval(function() {
            clearInterval(timer);
            control.className = "hidden";
        }, 3000);
    }
});
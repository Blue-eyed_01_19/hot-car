﻿var map;
var directionsService;
var renders = [];
var requests = [];
var routes = [];
var suggestedRoute = [];


function InitializeMap() {

    var latlng = new google.maps.LatLng(49.396675, 31.92627);

    var options = {
        zoom: 5,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map-canvas"), options);
}

function MapStart() {

    routes = [];
    requests = [];

    for (var a = 0; a < renders.length; a++) {
        renders[a].setMap(null);
    }

    renders = [];

    var newRoute = [];

    for (var i = 0, j = 0; i < suggestedRoute.length; i++) {
        if (suggestedRoute[i] != undefined && suggestedRoute[i] != "") {
            newRoute[j++] = suggestedRoute[i];
        }
    }

    if (newRoute.length > 1) {

        directionsService = new google.maps.DirectionsService();

        routes.push({ Points: newRoute });

        AddAutocomplete();

        ProcessRequests();
    }
}

function ProcessRequests() {

    for (var route = 0; route < routes.length; route++) {
        var wayPoints = [];

        var start, finish;

        var lastpoint;

        var data = routes[route].Points;

        for (var waypoint = 0; waypoint < data.length; waypoint++) {
            if (data[waypoint] === lastpoint) {
                continue;
            }

            lastpoint = data[waypoint];

            wayPoints.push({
                location: data[waypoint],
                stopover: true
            });
        }

        start = (wayPoints.shift()).location;
        finish = wayPoints.pop();

        if (finish === undefined) {
            finish = start;
        } else {
            finish = finish.location;
        }

        var request = {
            origin: start,
            destination: finish,
            waypoints: wayPoints,
            travelMode: google.maps.TravelMode.DRIVING
        };


        requests.push({ "route": route, "request": request });
    }

    ProcessRequest();
}

function ProcessRequest() {

    var i = 0;

    function submitRequest() {
        directionsService.route(requests[i].request, directionResults);
    }

    function directionResults(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {

            renders[i] = new google.maps.DirectionsRenderer(
            {
                suppressMarkers: false,
                suppressInfoWindows: false
            });

            renders[i].setDirections(result);
            renders[i].setMap(map);
        }

        nextRequest();
    }

    function nextRequest() {
        i++;

        if (i >= requests.length) {

            return;
        }

        submitRequest();
    }

    submitRequest();
}

function AddAutocomplete() {
    var autocompletePoints = [];

    $('[id^=wayPoint]').each(function () {
        autocompletePoints[$(this).attr('id').match(/\d+/)] = new google.maps.places.Autocomplete($(this)[0]);
    });

    $('[id^=wayPoint]').each(function () {
        var ind = $(this).attr('id').match(/\d+/);

        google.maps.event.addListener(autocompletePoints[ind], "place_changed", function () {
            suggestedRoute[ind] = document.getElementById("wayPoint" + ind).value;
            InitializeMap();
            MapStart();
        });
    });
}

google.maps.event.addDomListener(window, "load", function () {
    InitializeMap();
    AddAutocomplete();
});

$(function () {

    $('.autocomplete').on("keypress", function (e) {
        if (e.keyCode == 13) {
            $(this).trigger('blur');
        }
    });

    $(".autocomplete").blur(function () {
        var geocoder = new google.maps.Geocoder();
        var address = $(this);
        geocoder.geocode({ 'address': address.val() }, function (results, status) {
            if (status != google.maps.GeocoderStatus.OK) {
                address.val('');
                address.trigger('change');
            } else {
                AddAutocomplete();
            }
        });
    });

    $('[id^=wp]').each(function (i) {
        if (i > 0) {
            $(this).hide();
        }
    });

    $("#addWP").click(function () {

        if ($('[id^=wp]:hidden').length == 0)
            return false;

        $('[id^=wp]:hidden').first().show();

        AddAutocomplete();

        return false;
    });

    $(document).on('click', '#btnClose', function () {
        $(this).closest('[id^=wp]').hide();
        $(this).closest('[id ^= wp]').find('input').val('');

        $(this).closest('[id ^= wp]').find('input').trigger('change');

        return false;
    });

    $('[id^=wayPoint]').change(function () {
        if ($(this).val() == '') {
            var ind = $(this).attr('id').match(/\d+/);
            suggestedRoute[ind] = document.getElementById("wayPoint" + ind).value;
            MapStart();
            InitializeMap();
        }
    });


    $('#SubmitAndRedirect').click(function () {
        if (!validateForm()) {
            $('html, body').animate({
                scrollTop: $(".has-error").first().offset().top
            }, 200);
            return false;
        }
    });

    $('html, body').animate({
        scrollTop: $("#main").offset().top
    }, 800);


    $("#date").datepicker({ dateFormat: "dd/MM/yy", minDate: new Date() });


    $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            $("#btnFind").attr('value');
            return false;
        }
    });

    $('.date').change(function () {

        var $date = $('#date').val();
        var $hour = $('#hour').val();
        var $minute = $('#minute').val();

        $('#dateTime').val($date + " " + $hour + ":" + $minute);
    });
});

function validateForm() {
    clearErrors();
    var alertDiv = $.parseHTML("<div class='alert alert-danger fade in'><button aria-hidden='true' class='close' data-dismiss='alert' type='button'>×</button></div>");
    var result;

    if (($("#wayPoint0").val() != '') && ($("#wayPoint0").val() == $("#wayPoint7").val())) {
        $("#wayPoint0").parent('div').addClass('has-error');
        addAlert($("#wayPoint0").closest('.row'), $(alertDiv), "Пункт відправлення та прибуття не можуть містити одинакові значення!");

        $("#wayPoint7").parent('div').addClass('has-error');
        addAlert($("#wayPoint7").closest('.row'), $(alertDiv), "Пункт відправлення та прибуття не можуть містити одинакові значення!");

        result = false;
    }

    $('.validate').each(function () {
        if ($(this).val() == '' || (($(this).is('select')) && ($(this).find('option:selected').text() == ''))) {
            $(this).parent('div').addClass('has-error');
            addAlert($(this).closest('.row'), $(alertDiv), "Це поле є обов'язковим!");
            result = false;
        }
    });

    return result;
}

function addAlert(afterElem, alertDiv, info) {
    var alert = alertDiv.clone();
    alert.find('button').after(info);
    afterElem.after(alert);
}

function clearErrors() {
    $('.validate').each(function () {
        if ($(this).parent('div').hasClass('has-error')) {
            $(this).parent('div').removeClass('has-error');
            if ($(this).closest('.row').next().hasClass('alert')) {
                $(this).closest('.row').next().remove();
            }
        }
    });
}


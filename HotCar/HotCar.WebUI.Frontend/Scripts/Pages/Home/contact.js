﻿$(function () {
    (function scrollToTop() {
        $('html, body').animate({
            scrollTop: $("#main").offset().top
        }, 800);
    }())

    $("#contactForm").submit(function () {
        var result = true;
        $('.form-control').each(function () {
            if ($(this).parent().hasClass('has-error'))
            {
                result = false;
                return;
            }
        });

        if (result != false) {
            sendMail($("#contact_name").val(), $("#contact_email").val(), $("#contact_message").val());
        }
        return false;
    });

});

function sendMail(userName, userMail, msg) {
    $('#sendMsg').html("<img src='../Content/images/ajax-loader_small.gif' style='max-height: 100%; max-width: 100%'>");
    $('#sendMsg').prop('disabled', true);

    var mailObj = {
        Name: userName,
        MailAddress: userMail,
        Message: msg
    };

    var data = JSON.stringify({ 'mail': mailObj })


    $.ajax({
        type: "POST",
        url: "/Home/Contact",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            clearForm();
            $('#sendMsg').html("Відправити повідомлення");
            $('#sendMsg').prop('disabled', false);
            showAlert();   
        }
    });
}

function clearForm() {
    $("#contact_name").val('');
    $("#contact_email").val('');
    $("#contact_message").val('');
}

function showAlert() {
    $.bootstrapGrowl("Ваше повідомлення успішно відправлено!", {
        type: 'success',
        offset: { from: 'top', amount: 200 },
        align: 'center',
        width: 'auto',
        allow_dismiss: false,
        delay: 2000
    });
}
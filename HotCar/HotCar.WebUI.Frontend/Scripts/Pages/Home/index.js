﻿$(function () {
    if ($('#info_alert').val() != "") {
        $.bootstrapGrowl($('#info_alert').val(), {
            type: 'success',
            offset: { from: 'top', amount: 200 },
            align: 'center',
            width: 'auto',
            allow_dismiss: false,
            delay: 2000
        });
    }
})
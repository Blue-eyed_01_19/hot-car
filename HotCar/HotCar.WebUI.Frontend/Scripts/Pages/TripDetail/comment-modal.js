﻿
$(document).ready(function () {
    var loginLink = $("a[id='btn-show-modal']");
    loginLink.attr({ "href": "#", "data-toggle": "modal", "data-target": "#ModalComment" });

    DriverComments = function (driverLogin, currentUser) {
        if (driverLogin == currentUser) {
            $('#add-comment-box').addClass('hidden');
        } else {
            $('#add-comment-box').removeClass('hidden');
        }
        $.ajax({
            url: "/TripDetail/DriverComments",
            type: "GET",
            dataType: "json",
            data: { login: driverLogin }
        }).done(function (data) {
            $('.detailBox').html(data.htmlContent);
        });
    }

    PassengerComments = function (passengerLogin, driverLogin, currentUser) {
        if (driverLogin != currentUser) {
            $('#add-comment-box').addClass('hidden');
        } else {
            $('#add-comment-box').removeClass('hidden');
        }

        $.ajax({
            url: "/TripDetail/PassengerComments",
            type: "GET",
            dataType: "json",
            data: { login: passengerLogin}
        }).done(function (data) {
            $('.detailBox').html(data.htmlContent);
        });
    }
    
    addNewComment = function (tripId) {
        var text = document.getElementById("commentText");
        var rating = document.getElementById("input-rating");
        var userLogin = document.getElementById("user-comment-login").value;
        $.ajax({
            url: "/TripDetail/AddNewComment",
            type: "GET",
            dataType: "json",
            data: { tripId: tripId, commentText: text.value, about: userLogin, rating: rating.value }
        }).done(function(data) {
            $('.detailBox').html(data.htmlContent);
            text.value = "";         
        });
    }
});
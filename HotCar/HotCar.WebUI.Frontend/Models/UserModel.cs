﻿using System.Collections.Generic;
using HotCar.Entities;

namespace HotCar.WebUI.Frontend.Models
{
    public class UserModel
    {
        public User UserInfo { get; set; }
        public List<Trip> Trips { get; set; }
        public List<Comment> Comments { get; set; }

        #region Constructor

        public UserModel()
        {
            this.Trips = new List<Trip>();
        }

        #endregion
    }

}
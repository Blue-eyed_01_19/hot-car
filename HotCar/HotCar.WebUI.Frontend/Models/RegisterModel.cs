﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotCar.WebUI.Frontend.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Введіть ім'я")]
        [RegularExpression("^([АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюяA-Za-z]{1,10})$", ErrorMessage = "Невірне ім'я (1-10 букв)")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Введіть прізвище")]
        [RegularExpression("^([АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюяA-Za-z]{1,20})$", ErrorMessage = "Невірне прізвище (1-20 букв)")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Введіть логін")]
        [RegularExpression("^([АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюяA-Za-z](.){3,29})$", ErrorMessage = "Невірний логін (Перший символ - буква (4 - 30 символів))")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Введіть електронну пошту")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
                            ErrorMessage = "Невірна електронна пошта")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Введіть пароль")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6,ErrorMessage = "Довжина паролю повинна бути 6 - 20 символів")]
        public string Password { get; set; }


        [Required(ErrorMessage = "Повторіть пароль")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Паролі не збігаються")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Введіть телефон")]
        [RegularExpression("^\\+[0-9]{12}$", ErrorMessage = "Невірний формат. (+380...)")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        
        public DateTime? Birthday { get; set; }
     
        [Required(ErrorMessage = "Введіть дату")]
        [RegularExpression(@"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$",
                          ErrorMessage = "Невірний формат дати")]
        public string BirthdayString { get; set; }
    }
}
﻿using System.Collections.Generic;
using HotCar.Entities;

namespace HotCar.BLL.Abstract
{
    public interface IApplManager
    {
        void SignToTrip(Passenger passenger);
        bool ConfirmApplsForUser(Passenger passenger);
        List<Passenger> GetUnConfirmedPassengerToTrip(int tripId);
        List<Passenger> GetConfirmedPassengerToTrip(int tripId);
    }
}

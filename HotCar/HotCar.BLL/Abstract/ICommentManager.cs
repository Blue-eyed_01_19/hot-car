﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotCar.Entities;

namespace HotCar.BLL.Abstract
{
    public interface ICommentManager
    {
        List<Comment> GetDriverComments(int id);
        List<Comment> GetPassengerComments(int id);
        double GetDriverRating(int id);
        double GetPassengerRating(int id);
        List<Comment> GetAllMyComments(int id);
        List<Comment> GetAllAboutMeComments(int id);
        bool InsertComment(Comment comment);
        bool CreateNewComment(int tripId, string text, string to, string from, double rating);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotCar.BLL.Abstract;
using HotCar.Entities;
using HotCar.Repositories.Abstract;

namespace HotCar.BLL
{
    public class CommentManager : ICommentManager
    {
        #region Fields

        private ICommentRepository _commentRepository;
        private readonly ITripManager _tripManager;

        #endregion

        #region Constructors

        public CommentManager(ICommentRepository commentRepository, ITripManager tripManager)
        {          
            this._commentRepository = commentRepository;
            this._tripManager = tripManager;
        }

        #endregion

        #region Interface Implementation

        public List<Comment> GetDriverComments(int id)
        {
            return this._commentRepository.GetCommentsPassengersAboutDriverByDriverId(id);
        }

        public List<Comment> GetPassengerComments(int id)
        {
            return this._commentRepository.GetCommentsDriversAboutPassengerByPassengerId(id);
        }

        public double GetDriverRating(int id)
        {
            var rating = this._commentRepository.GetDriverRating(id);
            if (rating.Count > 0)
            {
                return Math.Round(rating.Average(), 2);
            }
            else
            {
                return 0;
            }
            
        }

        public double GetPassengerRating(int id)
        {
            var rating = this._commentRepository.GetPassengerRating(id);
            if (rating.Count > 0)
            {
                return Math.Round(rating.Average(), 2);
            }
            else
            {
                return 0;
            }
        }

        public List<Comment> GetAllMyComments(int id)
        {
            var comments =
                this._commentRepository.GetDriverCommentsByDriverId(id)
                    .Concat(this._commentRepository.GetPassengerCommentsByPassengerId(id))
                    .ToList();
            return comments;
          
        }

        public List<Comment> GetAllAboutMeComments(int id)
        {
            var comments =
                this._commentRepository.GetCommentsDriversAboutPassengerByPassengerId(id)
                    .Concat(this._commentRepository.GetCommentsPassengersAboutDriverByDriverId(id))
                    .ToList();
            return comments;
        } 

        public bool InsertComment(Comment comment)
        {
            return this._commentRepository.InsertNewComment(comment);
        }

        public bool CreateNewComment(int tripId, string text, string to, string from, double rating)
        {
            var trip = this._tripManager.GetTripById(tripId);
            if (from != trip.Driver.Login) //коментар про адміна
            {
                
                var comment = new Comment();
                comment.IsPassenger = true;
                comment.Text = text;
                comment.UserInfo.Login = from;
                comment.TripId = tripId;
                comment.Time = DateTime.Now;
                comment.Rating = rating;
                this.InsertComment(comment);
                return true;
            }
            else
            {
                var comment = new Comment();
                comment.IsPassenger =false ;
                comment.Text = text;
                comment.UserInfo.Login = to;
                comment.TripId = tripId;
                comment.Time = DateTime.Now;
                comment.Rating = rating;
                this.InsertComment(comment);
                return false;

            }


            
        }

        #endregion
    }
}

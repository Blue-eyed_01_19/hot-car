﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;
using ImapX;
using MailAddress = System.Net.Mail.MailAddress;


namespace HotCar.BLL.Services
{
    public class EmailService: IEmailService
    {
        #region Constants

        private const string ERROR_AUTHORIZATION = "error: unauthorized";

        #endregion


        #region Fields

        public string Email
        {
            get
            {
                SmtpClient smtp = new SmtpClient();
                return ((NetworkCredential) smtp.Credentials).UserName;
            }
        }

        #endregion


        #region Method

        public string GetMessageStatus()
        {
            ImapClient client = new ImapClient() { Host = ImapHost, UseSsl = ImapUseSsl, Port = ImapPort };
            client.Behavior.AutoPopulateFolderMessages = true;

            if (Connect(client))
            {
                string result = "( " + GetCountNewMessages(client) + " ) / " + GetCountInputMessages(client);
                client.Logout();
                return result;
            }

            return ERROR_AUTHORIZATION;
        }

        public void Send(string to, string message, string subject = "")
        {
            SmtpClient smtp = new SmtpClient();

            MailAddress mailAddress = new MailAddress(((NetworkCredential) smtp.Credentials).UserName, MailDisplayName);
            MailMessage mailMessage = new MailMessage(mailAddress.Address, to, subject, message);

            smtp.Send(mailMessage);
        }

        #endregion


        #region Helpers

        private bool Connect(ImapClient client)
        {
            if (client.Connect())
            {
                SmtpClient smtp = new SmtpClient();

                if (client.Login(((NetworkCredential)smtp.Credentials).UserName, ((NetworkCredential)smtp.Credentials).Password))
                {
                    return true;
                }
            }

            return false;
        }

        private string MailDisplayName
        {
            get { return WebConfigurationManager.AppSettings["MailDisplayName"]; }
        }

        private string ImapHost
        {
            get { return WebConfigurationManager.AppSettings["ImapHost"]; }
        }

        private bool ImapUseSsl
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings["ImapUseSsl"]); }
        }

        private int ImapPort
        {
            get { return Convert.ToInt32(WebConfigurationManager.AppSettings["ImapPort"]); }
        }

        private int GetCountNewMessages(ImapClient client)
        {
            return client.Folders["INBOX"].Search("UNSEEN").Length;
        }

        private int GetCountInputMessages(ImapClient client)
        {
            return client.Folders.Inbox.Messages.Count();
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using HotCar.Repositories.Abstract;
using System.Data.SqlClient;
using HotCar.Entities;


namespace HotCar.Repositories.Sql
{
    public class CommentRepository: Repository, IRepository<Comment>, ICommentRepository
    {
        #region Constructor

        public CommentRepository(string connectionString)
            : base(connectionString)
        {
        }

        #endregion Constructor

        #region ICommentRepository

        public Comment SelectInformation(SqlDataReader reader)
        {
            Comment comment = new Comment();

            comment.Id = (int) reader["Id"];
            comment.Time = (DateTime) reader["CommentTime"];
            comment.Text = (string) reader["CommentText"];
            comment.UserInfo.FirstName = (string)reader["FirstName"];
            comment.UserInfo.SurName = (string)reader["SurName"];
            comment.UserInfo.Login = (string) reader["SenderLogin"];
            comment.TripId = (int) reader["TripId"];
            
            return comment;
        }    

        #endregion

        #region ICommentRepository
        public List<Comment> GetCommentsDriversAboutPassengerByPassengerId(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_COMMENTS_DRIVERS_ABOUT_PASSENGER_BY_PASSENGER_ID,
                    connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.AddWithValue("@UserId", id);

                    List<Comment> comments = new List<Comment>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            comments.Add(this.SelectInformation(reader));
                        }

                        return comments;
                    }
                }
            }
        }

        public List<Comment> GetCommentsPassengersAboutDriverByDriverId(int id)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_COMMENTS_PASENGERS_ABOUT_DRIVER_BY_DRIVER_ID,
                    connection) { CommandType = CommandType.StoredProcedure })
                {

                    command.Parameters.AddWithValue("@UserId", id);

                    List<Comment> comments = new List<Comment>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            comments.Add(this.SelectInformation(reader));
                        }

                        return comments;
                    }
                }
            }
        }
            
        public List<Comment> GetPassengerCommentsByPassengerId(int id)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_PASSENGER_COMMENTS_BY_PASSENGER_ID,
                    connection) { CommandType = CommandType.StoredProcedure })
                {

                    command.Parameters.AddWithValue("@UserId", id);

                    List<Comment> comments = new List<Comment>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            comments.Add(this.SelectInformation(reader));
                        }

                        return comments;
                    }
                }
            }
        }

        public List<Comment> GetDriverCommentsByDriverId(int id)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_DRIVER_COMMENTS_BY_DRIVER_ID,
                    connection) { CommandType = CommandType.StoredProcedure })
                {

                    command.Parameters.AddWithValue("@UserId", id);

                    List<Comment> comments = new List<Comment>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            comments.Add(this.SelectInformation(reader));
                        }

                        return comments;
                    }
                }
            }
        }

        public List<double> GetDriverRating(int id)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_DRIVER_RATINGS,
                    connection) { CommandType = CommandType.StoredProcedure })
                {

                    command.Parameters.AddWithValue("@UserId", id);

                    List<double> ratings = new List<double>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ratings.Add((double) reader["Rating"]);
                        }

                        return ratings;
                    }
                }
            }
            
        }

        public List<double> GetPassengerRating(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_PASSENGER_RATINGS,
                    connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.AddWithValue("@UserId", id);

                    List<double> ratings = new List<double>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ratings.Add((double)reader["Rating"]);
                        }

                        return ratings;
                    }
                }
            }
        } 

        public bool InsertNewComment(Comment comment)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.INSERT_NEW_COMMENT,
                    connection) { CommandType = CommandType.StoredProcedure })
                {

                    command.Parameters.AddWithValue("@time", comment.Time);
                    command.Parameters.AddWithValue("@text", comment.Text);
                    command.Parameters.AddWithValue("@isPassenger", comment.IsPassenger);
                    command.Parameters.AddWithValue("@tripId", comment.TripId);
                    command.Parameters.AddWithValue("@login", comment.UserInfo.Login);
                    command.Parameters.AddWithValue("@rating", comment.Rating);

                    return Convert.ToBoolean(command.ExecuteNonQuery());
                }
            }
        }

        #endregion
    }
}

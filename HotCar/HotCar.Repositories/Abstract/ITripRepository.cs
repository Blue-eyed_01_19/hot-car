﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotCar.Entities;

namespace HotCar.Repositories.Abstract
{
    public interface ITripRepository
    {
        List<Trip> GetAllPassengerAvailableTrips(DateTime time);
        List<Trip> GetAllTripsByDriverId(int id);
        int GetCountConductedTripsByDriverId(int id);
        List<Trip> GetAllTripsByPassengerId(int id);
        List<Trip> GetActualTripsByDriverLogin(string login, DateTime dateTime);
        List<Trip> GetOutDatedTripsByDriverLogin(string login, DateTime dateTime);
        List<Trip> GetActualTripsByPassengerLogin(string login, DateTime dateTime, bool isConfirmed);
        List<Trip> GetOutDatedTripsByPassengerLogin(string login, DateTime dateTime, bool isConfirmed); 
        int GetCountConductedTripsByPassengerId(int id);
        bool DeleteTripById(int id);
        bool UpdateTripById(Trip trip);
        int InsertTrip(Trip tripInfo);
        bool AddRoute(LocationInfo location, int id);
        Trip GetTripById(int tripId);     //added
	    void SetAvailablePlacesCount(int tripId, byte availablePlacesCount);
    }
}

﻿using System.Collections.Generic;
using HotCar.Entities;

namespace HotCar.Repositories.Abstract
{
    public interface ICommentRepository
    {
        List<Comment> GetCommentsDriversAboutPassengerByPassengerId(int id);
        List<Comment> GetCommentsPassengersAboutDriverByDriverId(int id);
        List<double> GetDriverRating(int id);
        List<double> GetPassengerRating(int id);
        List<Comment> GetPassengerCommentsByPassengerId(int id);
        List<Comment> GetDriverCommentsByDriverId(int id);
        bool InsertNewComment(Comment comment);
    }
}

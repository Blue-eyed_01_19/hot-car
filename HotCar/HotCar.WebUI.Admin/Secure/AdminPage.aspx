﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" MasterPageFile="~/AdminPanel.Master" AutoEventWireup="true" CodeBehind="AdminPage.aspx.cs" Inherits="HotCar.WebUI.Admin.Secure.AdminPage" Culture="auto" UICulture="auto" %>

<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Scripts/Libs/ddaccordion.js"></script>
    <script type="text/javascript" src="../Scripts/ddaccordion_init.js"></script>
    <link href="../Styles/jq_styles.css" rel="stylesheet" />
    <link rel="icon" href="../App_Themes/Standart/images/logo.ico" type="image/x-icon"/>
    <script type="text/javascript" src="../Scripts/AdminPage.js"></script>
    <script src="../Scripts/Libs/jquery-1.8.2.js"></script>
    <script src="../Scripts/Libs/jquery-ui.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper2">
        <div class="section">
            <div class="content">
                <div class="left_content">
                    <div class="sidebar_search">
                        <div>
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="search_input" onclick="this.value=''"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" CssClass="search_submit" BorderStyle="None" OnClick="btnSearch_Click" />
                        </div>

                    </div>
                    <div class="clear"></div>
                    <div class="sidebarmenu">
                        <a class="menuitem submenuheader" href="">
                            <asp:Label ID="lblUsers" runat="server" Text="Користувачі"></asp:Label>
                        </a>
                        <div class="submenu">
                            <ul>
                                <li>
                                    <asp:Button ID="btnAdmins" runat="server" Text="Адміністратори" OnClick="btnAdmins_Click"/>

                                </li>
                                <li>
                                    <asp:Button ID="btnSimpleUsers" runat="server" Text="Звичайні користувачі" OnClick="btnSimpleUsers_Click" />

                                </li>
                                <li>
                                    <asp:Button ID="btnInactiveUsers" runat="server" Text="Неактивні" OnClick="btnInactiveUsers_Click" />
                                </li>
                                <li>
                                    <asp:Button ID="btnAllUsers" runat="server" Text="Усі користувачі" OnClick="btnAllUsers_Click" />
                                </li>
                            </ul>
                        </div>
                        <a class="menuitem submenuheader" href="">
                            <asp:Label ID="lblSettings" runat="server" Text="Налаштування"></asp:Label>
                        </a>
                        <div class="submenu">
                            <ul>
                                <li>
                                    <asp:Label ID="Label1" runat="server" Text="Користувачів на сторінці"></asp:Label>                          
                                    <asp:DropDownList ID="ddlGridViewPageSize" runat="server" CssClass="GridSettings" AutoPostBack="True" OnSelectedIndexChanged="ddlGridViewPageSize_SelectedIndexChanged">
                                        <asp:ListItem Value="10" Text="10 користувачів" ></asp:ListItem>
                                        <asp:ListItem Value="20" Text="20 користувачів" ></asp:ListItem>
                                        <asp:ListItem Value="50" Text="50 користувачів" ></asp:ListItem>
                                        <asp:ListItem Value="100" Text="100 користувачів"></asp:ListItem>
                                    </asp:DropDownList>
                                </li>
                            </ul>
                        </div>
                    </div>
             <%--       <div class="sidebar_box">
                        <div class="sidebar_box_top"></div>
                        <div class="sidebar_box_content">
                            <br/>
                            <div>
                                <asp:Label ID="lblMessages" CssClass="lblMessages" runat="server" Text="Повідомлення"></asp:Label>
                                <br/>
                                <img id="gmailImage" src="../App_Themes/Standart/images/gmail.png"/>
                                <span id="messagesInfo"></span>
                            </div>
                            <asp:Label runat="server" ID="email" CssClass="email"></asp:Label>
                            <br/>
                            <br/>
                        </div>
                        <div class="sidebar_box_bottom"></div>
                    </div>--%>

                    <div class="sidebar_box">
                        <div class="radio">
                            <a id="onlineRadioLink" href="http://radiotuna.com/classical-radio">classical radio</a><script type="text/javascript" src="http://radiotuna.com/OnlineRadioPlayer/EmbedRadio?playerParams=%7B%22styleSelection0%22%3A126%2C%22styleSelection1%22%3A178%2C%22styleSelection2%22%3A154%2C%22textColor%22%3A0%2C%22backgroundColor%22%3A13551045%2C%22buttonColor%22%3A5397227%2C%22glowColor%22%3A5397227%2C%22playerSize%22%3A200%2C%22playerType%22%3A%22style%22%7D&width=175&height=244"></script>
                        </div>
                    </div>

                   
                </div>


                <div class="right_content">
                    <h2>
                        <asp:Label ID="lblUserList" runat="server" Text="Список користувачів"></asp:Label>
                        (<asp:Label ID="lblUsersListName" runat="server"></asp:Label>)
                    </h2>
                    <div style="overflow-x: auto; width: 970px">
                        <asp:GridView ID="GridViewUsers" runat="server" CssClass="gridStyle" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="Vertical" Font-Size="Small" ShowFooter="True" AllowPaging="True" AutoGenerateEditButton="True" OnPageIndexChanging="GridViewUsers_PageIndexChanging" OnRowCancelingEdit="GridViewUsers_RowCancelingEdit" OnRowEditing="GridViewUsers_RowEditing" OnRowUpdating="GridViewUsers_RowUpdating" DataSourceID="odsGetUsers">

                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkRow" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ID">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblUserIdu" runat="server" Text='<%# Bind("Id") %>' ></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Логін">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblUserLoginu" runat="server" Text='<%# Bind("Login") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserLogin" runat="server" Text='<%# Bind("Login") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ім'я">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtUserNameu" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorFirstName"
                                            runat="server"
                                            ControlToValidate="txtUserNameu"
                                            ErrorMessage="Введіть ім'я"
                                            ForeColor="White"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorFirstName"
                                            runat="server"
                                            ErrorMessage="Невірне ім'я (1-10 букв)"
                                            ForeColor="White"
                                            ControlToValidate="txtUserNameu"
                                            ValidationExpression="^([АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюяA-Za-z]{1,10})$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Прізвище">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtUserSurnameu" runat="server" Text='<%# Bind("SurName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorSurName"
                                            runat="server"
                                            ControlToValidate="txtUserSurnameu"
                                            ErrorMessage="Введіть прізвище"
                                            ForeColor="White"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorSurName"
                                            runat="server"
                                            ErrorMessage="Невірне прізвище"
                                            ForeColor="White"
                                            ControlToValidate="txtUserSurnameu"
                                            ValidationExpression="^([АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюяA-Za-z]{1,20})$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserSurname" runat="server" Text='<%# Bind("SurName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Права">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlUserRole" runat="server" DataSourceID="odsUserRole" DataTextField="Value" DataValueField="Value">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserRole" runat="server" Text='<%# Bind("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Телефон">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtPhoneu" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPhone"
                                            runat="server"
                                            ControlToValidate="txtPhoneu"
                                            ErrorMessage="Введіть телефон"
                                            ForeColor="White"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone"
                                            runat="server"
                                            ErrorMessage="Невірний телефон"
                                            ForeColor="White"
                                            ControlToValidate="txtPhoneu"
                                            ValidationExpression="^\+([0-9]{12})$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Електронна пошта">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMailu" runat="server" Text='<%# Bind("Mail") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorMail"
                                            runat="server"
                                            ControlToValidate="txtMailu"
                                            ErrorMessage="Введіть електронну пошту"
                                            ForeColor="White"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorMail"
                                            runat="server"
                                            ErrorMessage="Невірний формат"
                                            ForeColor="White"
                                            ControlToValidate="txtMailu"
                                            ValidationExpression="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblMail" runat="server" Text='<%# Bind("Mail") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Дата народження">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtBirthdayu" runat="server" Text='<%# Convert.ToDateTime(Eval("Birthday")).ToString("d") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorBirthDate"
                                            runat="server"
                                            ControlToValidate="txtBirthdayu"
                                            ErrorMessage="Введіть дату народження"
                                            ForeColor="White"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorBirthDate"
                                            runat="server"
                                            ErrorMessage="Невірний формат дати"
                                            ForeColor="White"
                                            ControlToValidate="txtBirthdayu"
                                            ValidationExpression="^([0-9]{2})[.\/]([0-9]{2})[.\/]([0-9]{4})$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBirthday" runat="server" Text='<%# Convert.ToDateTime(Eval("Birthday")).ToString("d") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Про себе">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtAboutu" runat="server" Text='<%# Bind("AboutMe") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAbout" runat="server" Text='<%# Bind("AboutMe") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Неактивний">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtInactiveu" runat="server" Text='<%# Bind("Inactive") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblInactive" runat="server" Text='<%# Bind("Inactive") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <EmptyDataTemplate>
                                <asp:Label ID="lblSearchResults" runat="server" Text="Пошук не дав результату"></asp:Label>
                            </EmptyDataTemplate>
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" CssClass="gridHeader" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        </asp:GridView>

                    </div>
                    <asp:ObjectDataSource ID="odsUserRole" runat="server" SelectMethod="GetAllRoles" TypeName="HotCar.BLL.UsersManager"></asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="odsGetUsers" runat="server" TypeName="HotCar.BLL.UsersManager" DataObjectTypeName="HotCar.Entities.User" UpdateMethod="Update" SelectMethod="GetUsersByRole">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="all" Name="roles" Type="String" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Birthday" Type="DateTime" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    
                    <div class ="clear">
                    <button id="btnSendMsg" class="hiddenBtn" hidden="hidden">
                        <asp:Label ID="lblSendMessages" runat="server" Text="Надіслати повідомлення"></asp:Label></button>                                                      
                    <asp:Button runat="server" id="btnLockUsers" CssClass="hiddenBtn" hidden="hidden" Text="Заблокувати" OnClick="btnLockUsers_Click"></asp:Button>
                    <asp:Button runat="server" id="btnUnlockUser" CssClass="hiddenBtn" hidden="hidden" Text="Розблокувати" OnClick="btnUnlockUser_Click"></asp:Button>
                    </div>


                    <div id="SendMailDialog" title="Відправити повідомлення">
                        <label for="subject">Тема</label><br />
                        <input type="text" name="subject" id="subject" value="" style="width: 300px" /><br />
                        <label for="message">Повідомлення</label><br />
                        <textarea id="message" rows="5" style="width: 299px"></textarea>
                        <label id="valid-error" class="ui-state-error" hidden="hidden">Мінімальна довжина: 5 символів</label>
                    </div>

                    <div class="clear">
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</asp:Content>

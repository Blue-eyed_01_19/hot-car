﻿namespace HotCar.WebUI.Admin.Code.ExceptionWorking.Types
{
    public class UnexpectedErrorWorking : IWorkingExeption
    {
        public string WhatHappenedInfo
        {
            get { return "трапилась неочікувана помилка"; }
        }

        public string WhyHappenedInfo
        {
            get { return "причина невідома"; }
        }

        public string SuggestionInfo
        {
            get { return "почекати декілька хвилин і спробувати знову, або зв'язатись з адміністратором сайту"; }
        }
    }
}
﻿using System.Configuration;
using HotCar.BLL;
using HotCar.BLL.Abstract;
using HotCar.BLL.Services;
using HotCar.Repositories.Abstract;
using HotCar.Repositories.Sql;

namespace HotCar.WebUI.Admin.Code.Infrastructure
{
    public class NinjectModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<ISecurityManager>().To<SecurityManager>();
            Bind<IUsersManager>().To<UsersManager>();
            Bind<ITripManager>().To<TripManager>();
            Bind<ICommentManager>().To<CommentManager>();
            Bind<IApplManager>().To<ApplManager>();
            Bind<IEmailService>().To<EmailService>();

            Bind<IUserRepository>()
                .To<UserRepository>()
                .WithConstructorArgument("connectionString",
                    ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString); 
        }   
    }
}
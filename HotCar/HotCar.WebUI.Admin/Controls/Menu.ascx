﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menu.ascx.cs" Inherits="HotCar.WebUI.Admin.Controls.Menu" %>
    <div class="right_header">
        <asp:Label ID="lblRightSeparator" runat="server" Text="|"></asp:Label>
        <asp:LoginView ID="lv1" runat="server">    
            <LoggedInTemplate>
                <asp:Label ID="lblHello" runat="server" Text="Привіт"></asp:Label> 
                <asp:LoginName runat="server" ID="ln1" ForeColor="Orange" Font-Bold="True"/>,
                <asp:HyperLink ID="linkHome" Target="_blank" NavigateUrl="http://hotcarua-001-site1.myasp.net/" runat="server" Text="Відвідати сайт"/>|
            </LoggedInTemplate>
        </asp:LoginView><asp:LoginStatus runat="server" id="ls1"/>
        
    </div>

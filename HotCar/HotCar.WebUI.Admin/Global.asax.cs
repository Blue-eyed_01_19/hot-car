﻿using System;
using System.Configuration;
using System.Security.Principal;
using System.Web;
using System.Web.Optimization;
using System.Web.Security;
using HotCar.BLL;
using HotCar.Repositories.Sql;
using HotCar.WebUI.Admin.Code.Infrastructure;
using Ninject;
using Ninject.Web.Common;
using NLog;

namespace HotCar.WebUI.Admin
{
    public class Global : NinjectHttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;
                        bool status;

                        var securityManager =
                            new SecurityManager(
                                new UserRepository(
                                    ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString));

                        roles = securityManager.ReadUserRole(username, out status);

                        if (status)
                        {
                            securityManager.SignOut();
                        }
                        else
                        {
                            //Let us set the Pricipal with our user specific details
                            HttpContext.Current.User = new GenericPrincipal(
                              new GenericIdentity(username, "Forms"), roles.Split(';'));
                        }

                    }
                    catch (Exception)
                    {
                        //somehting went wrong
                    }
                }
            }

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                if (Server.GetLastError() != null)
                {
                    Exception ex = Server.GetLastError().GetBaseException();

                    if (ex is HttpException)
                    {

                        Server.Transfer("~/Pages/HttpErrorPage.aspx");
                    }
                    else
                    {
                        Server.Transfer("~/Pages/ExceptionErrorPage.aspx");
                        Logger logger = LogManager.GetCurrentClassLogger();
                        logger.Log(LogLevel.Error, ex);
                    }
                }
            }
            catch
            { }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected override IKernel CreateKernel()
        {
            IKernel kernel = new StandardKernel(new NinjectModule());
            return kernel;
        }
    }
}
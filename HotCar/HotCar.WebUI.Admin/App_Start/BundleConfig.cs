﻿using System.Web.Optimization;

namespace HotCar.WebUI.Admin
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            #region Scripts bundles

            #region Common Scripts

            bundles.Add(new ScriptBundle("~/bundles/common/scripts").Include(
                        "~/Scripts/Libs/jquery/jquery-1.9.1.js",
                         "~/Scripts/Libs/bootstrap/bootstrap.min.js",
                          "~/Scripts/Libs/plugins/retina.min.js",
                           "~/Scripts/Libs/jquery/plugins/jquery.validate.js",
                            "~/Scripts/Libs/bootstrap/jednotka.js"));

            #endregion

            #region Home Page

            bundles.Add(new ScriptBundle("~/bundles/HomePage/scripts").Include(
                        "~/Scripts/Libs/jquery/jquery.mobile.custom.min.js",
                         "~/Scripts/Libs/plugins/modernizr.custom.min.js",
                          "~/Scripts/Libs/jquery/plugins/jquery.isotope.min.js",
                           "~/Scripts/Libs/jquery/plugins/jquery.flexslider.min.js",
                            "~/Scripts/Libs/plugins/lightbox.min.js"));

            #endregion
          
            #region Login Page

            bundles.Add(new ScriptBundle("~/bundles/Login/scripts").Include(
                       "~/Scripts/Pages/Login/login-page.js"));

            #endregion

            #endregion

            #region Site Specific CSS

            bundles.Add(new StyleBundle("~/Content/jednotka/css").Include(
                     "~/Content/bootstrap/bootstrap.min.css",  
                     "~/Content/jednotka/jednotka_blue.css",
                     "~/Content/jednotka/demo.css"));

            bundles.Add(new StyleBundle("~/Content/jednotka/login/css").Include(
                     "~/Content/jednotka/Login/style.css",
                     "~/Content/jednotka/Login/form-elements.css"));
       
            #endregion

            BundleTable.EnableOptimizations = false;         
        }
    }
}
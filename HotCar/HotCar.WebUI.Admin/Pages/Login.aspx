﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPanel.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="HotCar.WebUI.Admin.Pages.Login" culture="auto" uiculture="auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="icon" href="../App_Themes/Standart/images/logo.ico" type="image/x-icon"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="register">    
        <li> 
          <h2 class="registration1">
              <asp:Login ID="loginForm" runat="server" OnAuthenticate="loginForm_Authenticate" FailureText="Невірний логін і/або пароль" LoginButtonText="Увійти" PasswordLabelText="Пароль" PasswordRequiredErrorMessage="Введіть пароль" RememberMeText="Запам'ятати мене" UserNameLabelText="Логін" UserNameRequiredErrorMessage="Введіть логін">
              </asp:Login>
          </h2>
        </li> 
    </ul> 
</asp:Content>

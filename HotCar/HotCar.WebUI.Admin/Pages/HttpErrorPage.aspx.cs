﻿using System;
using System.Web;
using System.Web.UI;
using HotCar.Entities.Enums;
using HotCar.WebUI.Admin.Code.Keys;

namespace HotCar.WebUI.Admin.Pages
{
    public partial class HttpErrorPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                HttpException ex = (HttpException)Server.GetLastError();
                lblErrorCode.Text = ex.GetHttpCode().ToString();
            }
            catch
            { }
        }
    }
}
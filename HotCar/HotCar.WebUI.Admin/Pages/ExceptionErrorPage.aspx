﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPanel.Master" AutoEventWireup="true" CodeBehind="ExceptionErrorPage.aspx.cs" Inherits="HotCar.WebUI.Admin.Pages.ExceptionErrorPage" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/Error/css/ExceptionError.css" rel="stylesheet" />
    <link rel="icon" href="../App_Themes/Standart/images/logo.ico" type="image/x-icon"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <img id="backgroundImage" src="../App_Themes/Error/images/error.png"/>
    <span id="errorInfoTitle">
        <asp:Label ID="lblSmthHappen" runat="server" Text="Щось пішло не так :("></asp:Label>
        <br/>
    </span>
    <div id="errorContent">
        <span id="textWhat">
            <asp:Label ID="lblError" runat="server" Text="Помилка: "></asp:Label></span>
        <asp:Label ID="lblWhat" CssClass="lblWhat" runat="server"></asp:Label>
        <span id="textWhy"><br/><br/><br/>
                <asp:Label ID="lblReason" runat="server" Text="Причина: "></asp:Label>
        </span>
        <asp:Label ID="lblWhy" runat="server" CssClass="lblWhy"></asp:Label>
        <span id="textSuggestion"><br/><br/><br/>
            <asp:Label ID="lblWhatToDo" runat="server" Text="Що робити: "></asp:Label>
        </span>
        <asp:Label ID="lblSuggestion" runat="server" CssClass="lblSuggestion"></asp:Label>
    </div>
</asp:Content>

﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using HotCar.BLL;
using HotCar.BLL.Abstract;
using HotCar.Entities.Enums;
using HotCar.Repositories.Sql;
using HotCar.WebUI.Admin.Code.Keys;
using Ninject;
using Ninject.Web;

namespace HotCar.WebUI.Admin.Pages
{
    public partial class Login : PageBase
    {
        #region Public Properties

        [Inject]
        public ISecurityManager SecurityManager { get; set; }
       
        #endregion

        #region Constructor
     
        #endregion
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (this.Page.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.SignOut();
                    this.Response.Redirect("~/Pages/Login.aspx");
                }
            }
        }

        protected void loginForm_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var userLogin = this.loginForm.UserName;
            var userPassword = this.loginForm.Password;
            var rememberUser = this.loginForm.RememberMeSet;

            if (this.SecurityManager.Authentication(userLogin, userPassword))
            {
                var returnUrl = this.Request.QueryString["ReturnUrl"] ?? "~/Secure/AdminPage.aspx";
                this.Response.Redirect(returnUrl);                
            }
                      
        }
    }
}
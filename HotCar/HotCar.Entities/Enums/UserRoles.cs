﻿namespace HotCar.Entities.Enums
{
    public enum UserRoles
    {
        User                = 0,
        Administrator       = 1,       
        Master              = 2
    }
}

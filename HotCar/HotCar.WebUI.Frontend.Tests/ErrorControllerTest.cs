﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using HotCar.WebUI.Frontend.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HotCar.WebUI.Frontend.Tests
{
    [TestClass]
    public class ErrorControllerTest
    {
        #region Test methods

        [TestMethod]
        public void ServerErrorTest()
        {
            ErrorController errorController = new ErrorController();
            var request = new HttpRequest("", "http://example.com/", "");
            var response = new HttpResponse(TextWriter.Null);
            response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
            var httpContext = new HttpContextWrapper(new HttpContext(request, response));
            errorController.ControllerContext = new ControllerContext(httpContext, new RouteData(), errorController);
          
            ViewResult viewResult = errorController.ServerError() as ViewResult;

            Assert.AreEqual("ServerError", viewResult.ViewName);
        }

        [TestMethod]
        public void NotFoundTest()
        {
            ErrorController errorController = new ErrorController();
            var request = new HttpRequest("", "http://examghvvj.com/", "");
            var response = new HttpResponse(TextWriter.Null);
            response.StatusCode = (int)System.Net.HttpStatusCode.NotFound;
            var httpContext = new HttpContextWrapper(new HttpContext(request, response));
            errorController.ControllerContext = new ControllerContext(httpContext, new RouteData(), errorController);
            
            ViewResult viewResult = errorController.NotFound() as ViewResult;

            Assert.AreEqual("NotFound", viewResult.ViewName);
        }

        [TestMethod]
        public void AccessDeniedTest()
        {
            ErrorController errorController = new ErrorController();
            var request = new HttpRequest("", "http://exam.com/", "");
            var response = new HttpResponse(TextWriter.Null);
            response.StatusCode = (int) System.Net.HttpStatusCode.Forbidden;
            var httpContext = new HttpContextWrapper(new HttpContext(request, response));
            errorController.ControllerContext = new ControllerContext(httpContext, new RouteData(), errorController);

            ViewResult viewResult = errorController.AccessDenied() as ViewResult;

            Assert.AreEqual("AccessDenied", viewResult.ViewName);
        }

        #endregion
    }
}

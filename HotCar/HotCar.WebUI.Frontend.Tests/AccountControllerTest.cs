﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using HotCar.BLL.Abstract;
using HotCar.Entities;
using HotCar.WebUI.Frontend.Controllers;
using HotCar.WebUI.Frontend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HotCar.WebUI.Frontend.Tests
{
    [TestClass]
    public class AccountControllerTest
    {
        #region Fields

        private Mock<ISecurityManager> _securityManagerMock;
        private Mock<ISecurityManager> _securityManagerMock2;
        private Mock<ISecurityManager> _securityManagerMock3;
        private Mock<IUsersManager> _usersManagerMock;
        private Mock<IUsersManager> _usersManagerMock2;
        private AccountController _accountController;
        private AccountController _accountController2;
        private AccountController _accountController3;

        #endregion

        #region TestMethods

        [TestInitialize]
        public void Initialize()
        {
            this._securityManagerMock = new Mock<ISecurityManager>();
            this._securityManagerMock.Setup((smm) => smm.Authentication("Admin", "Password"))
                .Returns(true);
            this._securityManagerMock.Setup((smm) => smm.CreateAccount(It.IsAny<User>()))
                .Returns(true);
            this._securityManagerMock2 = new Mock<ISecurityManager>();
            this._securityManagerMock2.Setup((smm) => smm.CreateAccount(It.IsAny<User>()))
                .Returns(false);
            this._securityManagerMock3 = new Mock<ISecurityManager>();
            this._securityManagerMock3.Setup((smm) => smm.CreateAccount(It.IsAny<User>()))
                .Returns((bool?)null);

            this._usersManagerMock = new Mock<IUsersManager>();
            this._usersManagerMock.Setup((umm) => umm.GetUserPhoto(It.IsAny<string>())).Returns(new UserPhoto());
            this._usersManagerMock2 = new Mock<IUsersManager>();
            UserPhoto photo = new UserPhoto();
            photo.FileExtension = "*.jpg";
            photo.Photo = new byte[] { 254 };
            this._usersManagerMock2.Setup((umm) => umm.GetUserPhoto(It.IsAny<string>())).Returns(photo);

            this._accountController = new AccountController(this._securityManagerMock.Object, this._usersManagerMock.Object);
            this._accountController2 = new AccountController(this._securityManagerMock2.Object, this._usersManagerMock.Object);
            this._accountController3 = new AccountController(this._securityManagerMock3.Object, this._usersManagerMock2.Object);
        }

        [TestMethod]
        public void LoginGetTest()
        {
            const string returnUrl = "/SuggestTrip/SuggestTrip";
            ViewResult viewResult = this._accountController.Login(returnUrl) as ViewResult;
            Assert.AreEqual(returnUrl, viewResult.ViewBag.ReturnUrl);
            Assert.AreEqual("Login", viewResult.ViewName);
        }

        [TestMethod]
        public void LoginPostCorrectLoginDataUrlNullTest()
        {
            String url = "Index/Home";

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            Mock<UrlHelper> urlHelperMock = new Mock<UrlHelper>(new RequestContext(httpContextBaseMock.Object, new RouteData()));
            urlHelperMock.Setup((uhm) => uhm.Action("Index", "Home")).Returns(url);

            LoginModel loginModel = new LoginModel();
            loginModel.UserName = "Admin";
            loginModel.Password = "Password";

            this._accountController.Url = urlHelperMock.Object;

            RedirectResult redirectResult = this._accountController.Login(loginModel, null) as RedirectResult;

            Assert.AreEqual("Index/Home", redirectResult.Url);
        }

        [TestMethod]
        public void LoginPostCorrectLoginDataUrlNotNullTest()
        {
            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            Mock<UrlHelper> urlHelperMock = new Mock<UrlHelper>(new RequestContext(httpContextBaseMock.Object, new RouteData()));
            urlHelperMock.Setup((uhm) => uhm.Action("Index", "Home")).Returns("Index/Home");

            LoginModel loginModel = new LoginModel();
            loginModel.UserName = "Admin";
            loginModel.Password = "Password";

            this._accountController.Url = urlHelperMock.Object;

            string url = "b/a";

            RedirectResult redirectResult = this._accountController.Login(loginModel, url) as RedirectResult;

            Assert.AreEqual("b/a", redirectResult.Url);
        }

        [TestMethod]
        public void LoginPostIncorrectLoginDataTest()
        {
            const string returnUrl = "/SuggestTrip/SuggestTrip";

            LoginModel loginModel = new LoginModel();
            loginModel.UserName = "roma";
            loginModel.Password = "123456";

            ViewResult viewResult =
                this._accountController.Login(loginModel, returnUrl) as ViewResult;

            Assert.IsTrue(this._accountController.ModelState.Values.Any(e => e.Errors.Count >= 1));
            Assert.AreEqual("Login", viewResult.ViewName);
        }

        [TestMethod]
        public void LoginPostInvalidModelStateTest()
        {
            const string returnUrl = "/SuggestTrip/SuggestTrip";

            LoginModel loginModel = new LoginModel();
            loginModel.UserName = "Admin";
            loginModel.Password = "Password";

            this._accountController.ModelState.AddModelError("Invalid", "Invalid state");
            ViewResult viewResult =
                this._accountController.Login(loginModel, returnUrl) as ViewResult;

            Assert.IsFalse(this._accountController.ModelState.IsValid);
            Assert.AreEqual("Login", viewResult.ViewName);
        }

        [TestMethod]
        public void LogoutTest()
        {
            const String action = "Index";
            const String controller = "Home";

            RedirectToRouteResult redirectToRouteResult = this._accountController.Logout() as RedirectToRouteResult;
            Assert.AreEqual(action, redirectToRouteResult.RouteValues["action"]);
            Assert.AreEqual(controller, redirectToRouteResult.RouteValues["controller"]);
        }

        [TestMethod]
        public void RegisterGetTest()
        {
            const string returnUrl = "/SuggestTrip/SuggestTrip";
            ViewResult viewResult = this._accountController.Register(returnUrl) as ViewResult;
            Assert.AreEqual(returnUrl, viewResult.ViewBag.ReturnUrl);
            Assert.AreEqual("Register", viewResult.ViewName);
        }

        [TestMethod]
        public void RegisterCorrectUrlNullTest()
        {
            String url = "Index/Home";

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            Mock<UrlHelper> urlHelperMock = new Mock<UrlHelper>(new RequestContext(httpContextBaseMock.Object, new RouteData()));
            urlHelperMock.Setup((uhm) => uhm.Action("Index", "Home", It.IsAny<object>())).Returns(url);

            RegisterModel registerModel = new RegisterModel();
            registerModel.BirthdayString = "12.12.1995";

            this._accountController.Url = urlHelperMock.Object;

            RedirectResult redirectResult = this._accountController.Register(registerModel, null) as RedirectResult;

            Assert.AreEqual("Index/Home", redirectResult.Url);
        }

        [TestMethod]
        public void RegisterCorrectUrlNotNullTest()
        {
            String url = "Index/Home";

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            Mock<UrlHelper> urlHelperMock = new Mock<UrlHelper>(new RequestContext(httpContextBaseMock.Object, new RouteData()));
            urlHelperMock.Setup((uhm) => uhm.Action("Index", "Home")).Returns(url);

            RegisterModel registerModel = new RegisterModel();
            registerModel.BirthdayString = "12.12.1995";

            this._accountController.Url = urlHelperMock.Object;

            string url2 = "b/a";

            RedirectResult redirectResult = this._accountController.Register(registerModel, url2) as RedirectResult;

            Assert.AreEqual("b/a", redirectResult.Url);
        }

        [TestMethod]
        public void RegisterPostSameLoginTest()
        {
            const string returnUrl = "/SuggestTrip/SuggestTrip";

            RegisterModel registerModel = new RegisterModel();
            registerModel.BirthdayString = "12.12.1995";

            ViewResult viewResult =
                this._accountController2.Register(registerModel, returnUrl) as ViewResult;

            Assert.IsTrue(this._accountController2.ModelState.Values.Any(e => e.Errors.Count >= 1));
            Assert.AreEqual("Register", viewResult.ViewName);
        }

        [TestMethod]
        public void RegisterPostSameMailTest()
        {
            const string returnUrl = "/SuggestTrip/SuggestTrip";

            RegisterModel registerModel = new RegisterModel();
            registerModel.BirthdayString = "12.12.1995";

            ViewResult viewResult =
                this._accountController3.Register(registerModel, returnUrl) as ViewResult;

            Assert.IsTrue(this._accountController3.ModelState.Values.Any(e => e.Errors.Count >= 1));
            Assert.AreEqual("Register", viewResult.ViewName);
        }


        [TestMethod]
        public void RegisterPostIncorrectDataTest()
        {
            const string returnUrl = "/SuggestTrip/SuggestTrip";

            this._accountController.ModelState.AddModelError("Error", "Error");

            ViewResult viewResult =
                this._accountController.Register(new RegisterModel(), returnUrl) as ViewResult;

            Assert.IsTrue(this._accountController.ModelState.Values.Any(e => e.Errors.Count >= 1));
            Assert.AreEqual("Register", viewResult.ViewName);
        }

        [TestMethod]
        public void UserPhotoNullTest()
        {
            const string path = "AccountControlles.cs";

            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.Server.MapPath(It.IsAny<String>())).Returns(path);
            this._accountController2.ControllerContext = controllerContext.Object;

            FileContentResult fileContentResult = this._accountController2.UserPhoto("login") as FileContentResult;

            Assert.IsTrue(fileContentResult == null);
        }

        [TestMethod]
        public void UserPhotoNotNullTest()
        {
            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.User.Identity.Name).Returns("userName");
            this._accountController3.ControllerContext = controllerContext.Object;

            FileContentResult fileContentResult = this._accountController3.UserPhoto("login") as FileContentResult;

            Assert.AreEqual(1, fileContentResult.FileContents.Length);
        }

        #endregion
    }
}

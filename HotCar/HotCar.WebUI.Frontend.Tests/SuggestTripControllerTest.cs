﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using HotCar.BLL.Abstract;
using HotCar.WebUI.Frontend.Code;
using HotCar.WebUI.Frontend.Controllers;
using HotCar.WebUI.Frontend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HotCar.WebUI.Frontend.Tests
{
    [TestClass]
    public class SuggestTripControllerTest
    {
        #region Fields

        private Mock<ITripManager> _tripManagerMock;
        private SuggestTripController _suggestTripController;
        private DirectionsRouteModel _directionsRouteModel;

        #endregion

        #region Test methods

        [TestInitialize]
        public void Initialize()
        {
            this._tripManagerMock = new Mock<ITripManager>();
            this._suggestTripController = new SuggestTripController(this._tripManagerMock.Object);

            this._directionsRouteModel = new DirectionsRouteModel();
            this._directionsRouteModel.WayPoints = new string[8];
            this._directionsRouteModel.WayPoints[0] = "First";
            this._directionsRouteModel.WayPoints[1] = "Intermediate";
            this._directionsRouteModel.WayPoints[7] = "Last";
        }
        

        [TestMethod]
        public void SuggestTripGetTest()
        {
            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.Session[SessionKeys.ROUTE]).Returns(null);
            this._suggestTripController.ControllerContext = controllerContext.Object;

            ViewResult viewResult = this._suggestTripController.SuggestTrip() as ViewResult;

            Assert.AreEqual("SuggestTrip", viewResult.ViewName);
        }

        [TestMethod]
        public void SuggestTripPostTest()
        {
            const string action = "SuggestTripNext";
            const string controller = "SuggestTrip";

            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.Session[SessionKeys.ROUTE]).Returns(this._directionsRouteModel);
            this._suggestTripController.ControllerContext = controllerContext.Object;

            RedirectToRouteResult redirectToRouteResult = this._suggestTripController.SuggestTrip(this._directionsRouteModel) as RedirectToRouteResult;

            Assert.AreEqual(action, redirectToRouteResult.RouteValues["action"]);
            Assert.AreEqual(controller, redirectToRouteResult.RouteValues["controller"]);
        }

        [TestMethod]
        public void SuggestTripNextGetTest()
        {
            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.Session[SessionKeys.ROUTE]).Returns(this._directionsRouteModel);
            this._suggestTripController.ControllerContext = controllerContext.Object;

            ViewResult viewResult = this._suggestTripController.SuggestTripNext() as ViewResult;
            
            Assert.AreEqual("SuggestTripNext", viewResult.ViewName);
        }

        [TestMethod]
        public void SuggestTripNextPostTest()
        {
            const string action = "Index";
            const string controller = "Home";

            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.Session[SessionKeys.ROUTE]).Returns(this._directionsRouteModel);
            controllerContext.Setup((cc) => cc.HttpContext.User.Identity.Name).Returns("userName");
            this._suggestTripController.ControllerContext = controllerContext.Object;

            RedirectToRouteResult redirectToRouteResult = this._suggestTripController.SuggestTripNext((DirectionsRouteModel)this._suggestTripController.Session[SessionKeys.ROUTE]) as RedirectToRouteResult;
            
            Assert.AreEqual(action, redirectToRouteResult.RouteValues["action"]);
            Assert.AreEqual(controller, redirectToRouteResult.RouteValues["controller"]);
        }

        #endregion
    }
}

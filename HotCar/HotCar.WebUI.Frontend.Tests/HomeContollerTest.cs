﻿using System;
using System.Web.Mvc;
using HotCar.BLL.Services;
using HotCar.WebUI.Frontend.Controllers;
using HotCar.WebUI.Frontend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HotCar.WebUI.Frontend.Tests
{
    [TestClass]
    public class HomeContollerTest
    {
        #region Test methods

        [TestMethod]
        public void IndexAlertNullTest()
        {
            HomeController homeController = new HomeController(new EmailService());
            ViewResult viewResult = homeController.Index() as ViewResult;
            Assert.AreEqual(viewResult.ViewBag.Alert, String.Empty);
            Assert.AreEqual("Index", viewResult.ViewName);
        }

        [TestMethod]
        public void IndexAlertNotNullTest()
        {
            HomeController homeController = new HomeController(new EmailService());
            ViewResult viewResult = homeController.Index("alert") as ViewResult;
            Assert.AreEqual(viewResult.ViewBag.Alert, "alert");
            Assert.AreEqual("Index", viewResult.ViewName);
        }

        [TestMethod]
        public void HowItWorksTest()
        {
            HomeController homeController = new HomeController(new EmailService());
            ViewResult viewResult = homeController.HowItWorks() as ViewResult;
            Assert.AreEqual("HowItWorks", viewResult.ViewName);
        }

        [TestMethod]
        public void FAQTest()
        {
            HomeController homeController = new HomeController(new EmailService());
            ViewResult viewResult = homeController.FAQ() as ViewResult;
            Assert.AreEqual("FAQ", viewResult.ViewName);
        }

        [TestMethod]
        public void ContactGetTest()
        {
            HomeController homeController = new HomeController(new EmailService());
            ViewResult viewResult = homeController.Contact() as ViewResult;
            Assert.AreEqual("Contact", viewResult.ViewName);
        }

        [TestMethod]
        public void ContactPostMailNullTest()
        {
            HomeController homeController = new HomeController(new EmailService());
            JsonResult jsonResult = homeController.Contact(null) as JsonResult;
            Assert.AreEqual("Failed", jsonResult.Data);
        }

        [TestMethod]
        public void ContactPostMailTest()
        {
            Mock<IEmailService> emailServiceMock = new Mock<IEmailService>();
            HomeController homeController = new HomeController(emailServiceMock.Object);
            MailMessageModel mailMessageModel = new MailMessageModel();
            mailMessageModel.Message = "Message";
            JsonResult jsonResult = homeController.Contact(mailMessageModel) as JsonResult;
            Assert.AreEqual("Success", jsonResult.Data);
        }

        #endregion
    }
}

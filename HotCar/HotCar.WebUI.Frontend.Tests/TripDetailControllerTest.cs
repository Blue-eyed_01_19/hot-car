﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using HotCar.BLL.Abstract;
using HotCar.Entities;
using HotCar.WebUI.Frontend.Controllers;
using HotCar.WebUI.Frontend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HotCar.WebUI.Frontend.Tests
{
    [TestClass]
    public class TripDetailControllerTest
    {
        #region Fields

        private Mock<ISecurityManager> _securityManagerMock;
        private Mock<ITripManager> _tripManagerMock;
        private Mock<IUsersManager> _userManagerMock;
        private Mock<ICommentManager> _commentManagerMock;
        private Mock<IApplManager> _applicationManagerMock;
        private TripDetailController _tripDetailController;

        private User _user;
        private Passenger _passenger;
        private Trip _trip;
        private Comment[] _comments;

        #endregion

        #region Test methods
       
        [TestInitialize]
        public void Initialize()
        {
            this._user = new User();
            this._user.Id = 1;
            this._user.Login = "Login";
            
            this._passenger = new Passenger();
            this._passenger.IsConfirmed = true;
            this._passenger.TripId = 1;
            this._passenger.UserInfo = this._user;
            this._passenger.Cost = 50;
            this._passenger.CountReservedSeats = 1;

            this._trip = new Trip();
            this._trip.Id = 1;
            this._trip.TripTime = DateTime.Now;
            this._trip.LocationAddresses = new List<string>();
            this._trip.LocationAddresses.Add("Kyiv");
            this._trip.LocationAddresses.Add("Lviv");
            this._trip.Passangers = new List<Passenger>();
            this._trip.AvailablePlacesCount = 8;
            this._trip.AdditionalInfo = "trip ai";

            this._comments = new Comment[2];
            this._comments[0] = new Comment();
            this._comments[0].Text = "comment user";
            this._comments[0].Time = DateTime.Now;
            this._comments[0].ParticipantId = 1;
            this._comments[0].Id = 1;
            this._comments[0].UserInfo = this._user;
            
            this._comments[1] = new Comment();
            this._comments[1].Text = "comment driver";
            this._comments[1].Time = DateTime.Now;
            this._comments[1].ParticipantId = 1;
            this._comments[1].Id = 1;
            this._comments[1].UserInfo = this._user;
            
            this._securityManagerMock = new Mock<ISecurityManager>();
            this._securityManagerMock.Setup((smm) => smm.GetName()).Returns(this._user.Login);

            this._userManagerMock = new Mock<IUsersManager>();
            this._userManagerMock.Setup((umm) => umm.GetUser(It.IsAny<String>())).Returns(this._user);

            this._tripManagerMock = new Mock<ITripManager>();
            this._tripManagerMock.Setup((tmm) => tmm.GetTripById(It.IsAny<int>())).Returns(this._trip);

            List<Comment> userComments = new List<Comment>();
            userComments.Add(this._comments[0]);

            List<Comment> driverComments = new List<Comment>();
            driverComments.Add(this._comments[1]);

            this._commentManagerMock = new Mock<ICommentManager>();
            this._commentManagerMock.Setup((cmm) => cmm.GetPassengerComments(It.IsAny<int>())).Returns(userComments);
            this._commentManagerMock.Setup((cmm) => cmm.GetDriverComments(It.IsAny<int>())).Returns(driverComments);
            this._commentManagerMock.Setup((cmm) => cmm.CreateNewComment(It.IsAny<int>(), It.IsAny<string>(), "Login", It.IsAny<string>(), It.IsAny<double>()))
                .Returns(true);
            this._commentManagerMock.Setup((cmm) => cmm.CreateNewComment(It.IsAny<int>(), It.IsAny<string>(), "Login2", It.IsAny<string>(), It.IsAny<double>()))
               .Returns(false);

            this._applicationManagerMock = new Mock<IApplManager>();

            this._tripDetailController = new TripDetailController(this._userManagerMock.Object,
                this._tripManagerMock.Object, this._commentManagerMock.Object, this._securityManagerMock.Object,this._applicationManagerMock.Object);
        }

        [TestMethod]
        public void GetTripInfoTest()
        {
            this._tripManagerMock.Setup((tmm) => tmm.ClearPassengersFromTrip(It.IsAny<Trip>(), false));
            this._commentManagerMock.Setup((cmm) => cmm.GetDriverRating(It.IsAny<int>())).Returns(4);
            ViewResult viewResult = this._tripDetailController.GetTripInfo(1) as ViewResult;

            Assert.AreEqual(this._user.Id, (viewResult.Model as UserModel).UserInfo.Id);
            Assert.AreEqual(this._user.Login, (viewResult.Model as UserModel).UserInfo.Login);
            Assert.AreEqual(1, (viewResult.Model as UserModel).Trips.Count);
            Assert.AreEqual(this._trip.Id, (viewResult.Model as UserModel).Trips.First().Id);
            Assert.AreEqual("GetTripInfo", viewResult.ViewName);
        }

        [TestMethod]
        public void PassengerCommentsTest()
        {
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "TripDetailController");
           
            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            
            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.SetupGet((ccm) => ccm.RouteData).Returns(routeData);
            controllerContextMock.Setup((ccm) => ccm.Controller).Returns(this._tripDetailController);
            controllerContextMock.Setup((ccm) => ccm.HttpContext).Returns(httpContextBaseMock.Object);
            this._tripDetailController.ControllerContext = controllerContextMock.Object;
            
            Mock<IView> viewMock = new Mock<IView>();
            Mock<IViewEngine> viewEngineMock = new Mock<IViewEngine>();
            Mock<ViewEngineResult> viewEngineResultMock = new Mock<ViewEngineResult>(viewMock.Object, viewEngineMock.Object);
            viewEngineMock.Setup(
                (vem) => vem.FindPartialView(It.IsAny<ControllerContext>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(viewEngineResultMock.Object);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(viewEngineMock.Object);

            JsonResult jsonResult = this._tripDetailController.PassengerComments("Login") as JsonResult;

            Assert.AreNotEqual(null, jsonResult.Data);
        }


        [TestMethod]
        public void DriverCommentsTest()
        {
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "TripDetailController");

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.SetupGet((ccm) => ccm.RouteData).Returns(routeData);
            controllerContextMock.Setup((ccm) => ccm.Controller).Returns(this._tripDetailController);
            controllerContextMock.Setup((ccm) => ccm.HttpContext).Returns(httpContextBaseMock.Object);
            this._tripDetailController.ControllerContext = controllerContextMock.Object;

            Mock<IView> viewMock = new Mock<IView>();
            Mock<IViewEngine> viewEngineMock = new Mock<IViewEngine>();
            Mock<ViewEngineResult> viewEngineResultMock = new Mock<ViewEngineResult>(viewMock.Object, viewEngineMock.Object);
            viewEngineMock.Setup(
                (vem) => vem.FindPartialView(It.IsAny<ControllerContext>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(viewEngineResultMock.Object);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(viewEngineMock.Object);

            JsonResult jsonResult = this._tripDetailController.DriverComments("Login") as JsonResult;

            Assert.AreNotEqual(null, jsonResult.Data);
        }


        [TestMethod]
        public void AddNewCommentPassengerTest()
        {
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "TripDetailController");

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.SetupGet((ccm) => ccm.RouteData).Returns(routeData);
            controllerContextMock.Setup((ccm) => ccm.Controller).Returns(this._tripDetailController);
            controllerContextMock.Setup((ccm) => ccm.HttpContext).Returns(httpContextBaseMock.Object);
            this._tripDetailController.ControllerContext = controllerContextMock.Object;

            Mock<IView> viewMock = new Mock<IView>();
            Mock<IViewEngine> viewEngineMock = new Mock<IViewEngine>();
            Mock<ViewEngineResult> viewEngineResultMock = new Mock<ViewEngineResult>(viewMock.Object, viewEngineMock.Object);
            viewEngineMock.Setup(
                (vem) => vem.FindPartialView(It.IsAny<ControllerContext>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(viewEngineResultMock.Object);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(viewEngineMock.Object);

            JsonResult jsonResult = this._tripDetailController.AddNewComment(1, "go go go", "Login2", 4) as JsonResult;

            Assert.AreNotEqual(null, jsonResult.Data);
        }

        [TestMethod]
        public void AddNewCommentDriverTest()
        {
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "TripDetailController");

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.SetupGet((ccm) => ccm.RouteData).Returns(routeData);
            controllerContextMock.Setup((ccm) => ccm.Controller).Returns(this._tripDetailController);
            controllerContextMock.Setup((ccm) => ccm.HttpContext).Returns(httpContextBaseMock.Object);
            this._tripDetailController.ControllerContext = controllerContextMock.Object;

            Mock<IView> viewMock = new Mock<IView>();
            Mock<IViewEngine> viewEngineMock = new Mock<IViewEngine>();
            Mock<ViewEngineResult> viewEngineResultMock = new Mock<ViewEngineResult>(viewMock.Object, viewEngineMock.Object);
            viewEngineMock.Setup(
                (vem) => vem.FindPartialView(It.IsAny<ControllerContext>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(viewEngineResultMock.Object);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(viewEngineMock.Object);

            JsonResult jsonResult = this._tripDetailController.AddNewComment(1, "go go go", "Login", 4) as JsonResult;

            Assert.AreNotEqual(null, jsonResult.Data);
        }

        [TestMethod]
        public void SignToTripNoSuccessTest()
        {
            this._user.Birthday = DateTime.Now.AddYears(-10);

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.Setup((hcbm) => hcbm.User.Identity.Name).Returns("SomeLogin");

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup((ccm) => ccm.Controller).Returns(this._tripDetailController);
            controllerContextMock.Setup((ccm) => ccm.HttpContext).Returns(httpContextBaseMock.Object);
            this._tripDetailController.ControllerContext = controllerContextMock.Object;

            JsonResult jsonResult = this._tripDetailController.SignToTrip(1, 2, 3) as JsonResult;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JsonResultData jsonResultData = serializer.Deserialize<JsonResultData>(serializer.Serialize(jsonResult.Data));

            Assert.AreEqual(false, jsonResultData.Success);
            Assert.IsTrue(jsonResultData.ApplMessage.Length > 0);
        }

        [TestMethod]
        public void SignToTripNoSuccess2Test()
        {
            this._user.Birthday = new DateTime(1990, 10, 10);
            this._trip.Driver = this._user;

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.Setup((hcbm) => hcbm.User.Identity.Name).Returns(this._user.Login);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup((ccm) => ccm.Controller).Returns(this._tripDetailController);
            controllerContextMock.Setup((ccm) => ccm.HttpContext).Returns(httpContextBaseMock.Object);
            this._tripDetailController.ControllerContext = controllerContextMock.Object;

            JsonResult jsonResult = this._tripDetailController.SignToTrip(1, 2, 3) as JsonResult;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JsonResultData jsonResultData = serializer.Deserialize<JsonResultData>(serializer.Serialize(jsonResult.Data));

            Assert.AreEqual(false, jsonResultData.Success);
            Assert.IsTrue(jsonResultData.ApplMessage.Length > 0);
        }

        [TestMethod]
        public void SignToTripNoSuccess3Test()
        {
            this._user.Birthday = new DateTime(1990, 10, 10);
            this._trip.AvailablePlacesCount = 1;
            this._passenger.CountReservedSeats = 2;

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.Setup((hcbm) => hcbm.User.Identity.Name).Returns(this._user.Login);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup((ccm) => ccm.Controller).Returns(this._tripDetailController);
            controllerContextMock.Setup((ccm) => ccm.HttpContext).Returns(httpContextBaseMock.Object);
            this._tripDetailController.ControllerContext = controllerContextMock.Object;

            JsonResult jsonResult = this._tripDetailController.SignToTrip(1, 2, 3) as JsonResult;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JsonResultData jsonResultData = serializer.Deserialize<JsonResultData>(serializer.Serialize(jsonResult.Data));

            Assert.AreEqual(false, jsonResultData.Success);
            Assert.IsTrue(jsonResultData.ApplMessage.Length > 0);
        }

        [TestMethod]
        public void SignToTripSuccessTest()
        {
            this._user.Birthday = new DateTime(1990, 10, 10);
            this._trip.AvailablePlacesCount = 3;
            this._passenger.CountReservedSeats = 2;

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.Setup((hcbm) => hcbm.User.Identity.Name).Returns(this._user.Login);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup((ccm) => ccm.Controller).Returns(this._tripDetailController);
            controllerContextMock.Setup((ccm) => ccm.HttpContext).Returns(httpContextBaseMock.Object);
            this._tripDetailController.ControllerContext = controllerContextMock.Object;

            JsonResult jsonResult = this._tripDetailController.SignToTrip(1, 2, 3) as JsonResult;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JsonResultData jsonResultData = serializer.Deserialize<JsonResultData>(serializer.Serialize(jsonResult.Data));

            Assert.AreEqual(true, jsonResultData.Success);
            Assert.IsTrue(jsonResultData.ApplMessage.Length > 0);
        }

        #endregion

        private class JsonResultData
        {
            public bool Success { get; set; }
            public String ApplMessage { get; set; }
        }
    }
}

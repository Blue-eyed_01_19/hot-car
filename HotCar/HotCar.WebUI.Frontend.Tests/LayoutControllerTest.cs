﻿using System;
using System.Net;
using System.Net.Mime;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using HotCar.BLL.Abstract;
using HotCar.WebUI.Frontend.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Security.Principal;
using System.Web.Hosting;
using System.Web.Mvc;
using HotCar.Entities;
using HotCar.WebUI.Frontend.Code;

namespace HotCar.WebUI.Frontend.Tests
{
    [TestClass]
    public class LayoutControllerTest
    {
        #region Fields

        private Mock<ISecurityManager> _securityManagerMock;
        private Mock<IUsersManager> _usersManagerMock;
        private Mock<IUsersManager> _usersManagerMock2;
        private LayoutController _layoutController;
        private LayoutController _layoutController2;

        #endregion

        #region Test methods

        [TestInitialize]
        public void Initialize()
        {
            this._securityManagerMock = new Mock<ISecurityManager>();
            this._usersManagerMock = new Mock<IUsersManager>();
            this._usersManagerMock.Setup((umm) => umm.GetUserPhoto(It.IsAny<string>())).Returns(new UserPhoto());
            this._usersManagerMock2 = new Mock<IUsersManager>();
            UserPhoto photo = new UserPhoto();
            photo.FileExtension = "*.jpg";
            photo.Photo = new byte[] { 234 };
            this._usersManagerMock2.Setup((umm) => umm.GetUserPhoto(It.IsAny<string>())).Returns(photo);

            this._layoutController = new LayoutController(this._securityManagerMock.Object,
                this._usersManagerMock.Object);

            this._layoutController2 = new LayoutController(this._securityManagerMock.Object,
                this._usersManagerMock2.Object);
        }

        [TestMethod]
        public void UserPhotoNullTest()
        {
            const string path = "LayoutControlles.cs";

            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.User.Identity.Name).Returns("userName");
            controllerContext.Setup((cc) => cc.HttpContext.Server.MapPath(It.IsAny<String>())).Returns(path);
            this._layoutController.ControllerContext = controllerContext.Object;

            FileContentResult fileContentResult = this._layoutController.UserPhoto() as FileContentResult;

            Assert.IsTrue(fileContentResult == null);
        }

        [TestMethod]
        public void UserPhotoNotNullTest()
        {
            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.User.Identity.Name).Returns("userName");
            this._layoutController2.ControllerContext = controllerContext.Object;

            FileContentResult fileContentResult = this._layoutController2.UserPhoto() as FileContentResult;

            Assert.AreEqual(1, fileContentResult.FileContents.Length);
        }

        [TestMethod]
        public void GetUserPhoto()
        {
            Mock<ControllerContext> controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup((cc) => cc.HttpContext.User.Identity.Name).Returns("userName");
            this._layoutController2.ControllerContext = controllerContext.Object;

            JsonResult jsonResult = this._layoutController2.GetUserPhoto() as JsonResult;

            Assert.IsTrue(jsonResult.Data != null);
        }

        #endregion
    }
}
